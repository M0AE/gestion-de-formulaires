# Gestion des formulaires

Ce projet est une application qui fait de la gestion de formulaires et qui utilise les technologies suivantes : 

React,
Tailwind CSS,
Node.js,
Express.js

La consigne de base était de créer une application web en PHP, JavaScript, CSS et HTML, en connectant une base de données et en manipulant les données via un CRUD. Très rapidement, j'ai réalisé que ces technologies n'étaient pas les plus performantes pour le projet. C'est pourquoi, avec détermination et ambition, j'ai choisi les technologies mentionnées ci-dessus. La prise en main a été initialement fastidieuse, et le projet a été repris à zéro plusieurs fois. Cependant, à travers ce projet, je n'ai cessé de m'améliorer. Comme vous pourrez le constater vous-même, j'ai encore énormément de choses à apprendre. Surtout, il me faudra maîtriser ce que j'ai appris pour mes futurs projets, car je ne compte pas abandonner ces technologies.

## Installation

Pour démarrer l'application, suivez ces étapes aprés avoir git clone le project dans votre ide :

### Client

1. Accédez au dossier client :
    ```
    cd gestion_formulaires/client
    ```

2. Installez les dépendances :
    ```
    npm install
    ```

3. Lancez l'application client :
    ```
    npm start
    ```

### Serveur

Assurez-vous d'avoir Node.js installé au préalable.

1. Accédez au dossier du serveur :
    ```
    cd gestion_formulaires/server
    ```

2. Lancez le serveur :
    ```
    npm start
    ```

## Base de données

Dans le dossier `server`, vous trouverez un répertoire nommé `sql` contenant un fichier `db.sql`. Ce fichier contient la structure de la base de données.

### Importation dans phpMyAdmin

1. Copiez le contenu du fichier `db.sql`.
2. Accédez à phpMyAdmin.
3. Créez une nouvelle base de données avec le nom de votre choix.
4. Collez le contenu de `db.sql` dans la fenêtre SQL de phpMyAdmin.
5. Vérifiez la connexion à la base de données dans le fichier `server.js`.

Assurez-vous que votre environnement dispose des autorisations et des configurations nécessaires pour exécuter l'application.
Notamment en ayant téléchargé Node.js dans sa version la plus récente, ainsi que npm.

### Essais de CRUD

Vous pourrez voir côtés client un fichier CrudClient.txt qui montre mon essai de crud sur le client.

Vous pourrez voir côtés server dans server.js un commentaire //Logique CRUD ligne 223. Ce sont mes rêquetes que j'ai mis en place pour le CRUD , malheuresement je n'ai pas réussi à finaliser ce CRUD mais les rêquetes sont présentes.
