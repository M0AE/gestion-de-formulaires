/*
	Auteur : Rosat Aurélien
	Date : 13/12/2023
	Description : Base de Données
*/

CREATE TABLE user (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(50),
    prenom VARCHAR(50),
    mot_de_passe VARCHAR(100),
    email VARCHAR(100)
);

CREATE TABLE contract (
    contractId INT PRIMARY KEY AUTO_INCREMENT,
    userId INT,
    date DATE,
    nbCopie INT,
    texte1 TEXT,
    texte2 TEXT,
    texte3 TEXT,
    date2 DATE,
    texte4 TEXT,
    nbPartenairesR INT,
    tempsNC TEXT,
    pays VARCHAR(255),
    heureSignature TEXT,
    nomAvocat VARCHAR(255),
    FOREIGN KEY (userId) REFERENCES user(id)
);

CREATE TABLE partenaire (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(50),
    prenom VARCHAR(50),
    email VARCHAR(50),
    contractId INT,
    FOREIGN KEY (contractId) REFERENCES contract(contractId)
);

CREATE TABLE contribution (
    id INT PRIMARY KEY AUTO_INCREMENT,
    texte TEXT,
    contractId INT,
    FOREIGN KEY (contractId) REFERENCES contract(contractId)
);

