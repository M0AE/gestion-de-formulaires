/*
	Auteur : Rosat Aurélien
	Date : 13/12/2023
	Description : Connection Base de Données + Requêtes
*/

const express = require('express')
const bcrypt = require('bcrypt')
const mysql = require('mysql')
const cors = require('cors')
const app = express()

app.use(express.json())

//Pour eviter les probléme de navigateur
const corsOptions = {
  origin: 'http://localhost:3000', // Remplace avec l'origine de ton client
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  preflightContinue: false,
  optionsSuccessStatus: 204,
  allowedHeaders: 'Content-Type,Authorization',
};
// Middleware CORS personnalisé
const customCors = (req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000'); // Remplace avec l'URL de ton client
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
};

app.use(customCors);
app.use(cors(corsOptions));
app.use(cors());

//DB Connection
const db= mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "formulaire"
})

//route par défaut pour voir si le server tourne correctement
app.get('/', (req,res)=>{
    return res.json("Connected to Backend");
})

const salt = 10; // définit le nombre de tours de chiffrement de bcrypt

// registration
app.post('/register', (req, res) => {
  const { nom, prenom, mdp, email } = req.body; 

  console.log("Données reçues depuis le formulaire :", req.body); // Vérification des données reçues

  bcrypt.hash(mdp.toString(), salt, (err, hash) => {
    if (err) {
      console.error("Erreur de hachage du mot de passe :", err);
      return res.status(500).json({ error: "Error hashing password" });
    }

    const values = [nom, prenom, hash, email];
    
    const sql = "INSERT INTO `user` (`nom`, `prenom`, `mot_de_passe`, `email`) VALUES (?)";

    db.query(sql, [values], (err, result) => {
      if (err) {
        console.error("Erreur lors de l'insertion des données :", err);
        return res.status(500).json({ error: "Error inserting data in server" });
      }

      console.log("Insertion réussie :", result);
      return res.json({ status: "Success" });
    });
  });
});

// authentification
app.post('/login', (req, res) => {
  const sql = 'SELECT * FROM user WHERE `email` = ?';
  db.query(sql, [req.body.email], (err, rows) => {
    if (err) return res.status(500).json({ error: "Login Error in server" });

    if (rows.length > 0) {
      const userData = rows[0];
      const storedPassword = userData && userData.mot_de_passe ? userData.mot_de_passe.toString().trim() : null;

      console.log("Stored Password:", storedPassword);

      if (storedPassword) {
        bcrypt.compare(req.body.mdp.toString(), storedPassword, (err, response) => {
          if (err) return res.json({ error: "Error comparing passwords" });

          console.log("Response from bcrypt compare:", response);

          if (response) {
            return res.json({ Status: "Success" });
          } else {
            return res.json({ Error: "Password does not match" });
          }
        });
      } else {
        return res.json({ Error: "Password is not defined or empty" });
      }
    } else {
      return res.json({ Error: "No email existed" });
    }
  });
});

// Route pour l'insertion dans la table contract
app.post('/contract', (req, res) => {
  const valuesCData = req.body.values;
  const insertQuery = 'INSERT INTO `contract` (`date`, `nbCopie`, `texte1`, `texte2`, `texte3`, `date2`, `texte4`, `nbPartenairesR`, `tempsNC`, `pays`, `heureSignature`,`nomAvocat`)  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
  const insertValues = [
      valuesCData.date,
      valuesCData.nbCopie,
      valuesCData.texte1,
      valuesCData.texte2,
      valuesCData.texte3,
      valuesCData.date2,
      valuesCData.texte4,
      valuesCData.nbPartenairesR,
      valuesCData.tempsNC,
      valuesCData.pays,
      valuesCData.heureSignature,
      valuesCData.nomAvocat
  ];

  db.query(insertQuery, insertValues, (err, result) => {
      if (err) {
          console.error('Erreur lors de l\'insertion des données dans la table contract :', err);
          res.status(500).send('Erreur lors de l\'insertion des données dans la table contract');
      } else {
          console.log('Données de valuesC insérées dans la table contract avec succès.');
          res.status(200).send('Données de valuesC insérées dans la table contract avec succès.');
      }
  });
});

// Route pour l'insertion dans la table partenaire
app.post('/partner', (req, res) => {
  const valuesPData = req.body.values;
  const insertQuery = 'INSERT INTO `partenaire` (`nom`, `prenom`, `email`) VALUES (?, ?, ?)';
  const insertValues = [valuesPData.nom, valuesPData.prenom, valuesPData.email];

  db.query(insertQuery, insertValues, (err, result) => {
      if (err) {
          console.error('Erreur lors de l\'insertion des données dans la table partenaire :', err);
          res.status(500).send('Erreur lors de l\'insertion des données dans la table partenaire');
      } else {
          console.log('Données de valuesP insérées dans la table partenaire avec succès.');
          res.status(200).send('Données de valuesP insérées dans la table partenaire avec succès.');
      }
  });
});

// Route pour l'insertion dans la table contribution
app.post('/contribution', (req, res) => {
  const valuesCOData = req.body.values;
  const insertQuery = 'INSERT INTO `contribution` (`texte`) VALUES (?)';
  const insertValues = [valuesCOData.text];

  db.query(insertQuery, insertValues, (err, result) => {
      if (err) {
          console.error('Erreur lors de l\'insertion des données dans la table contribution :', err);
          res.status(500).send('Erreur lors de l\'insertion des données dans la table contribution');
      } else {
          console.log('Données de valuesCO insérées dans la table contribution avec succès.');
          res.status(200).send('Données de valuesCO insérées dans la table contribution avec succès.');
      }
  });
});


// récupération des contracts d'un user particulier.

// Route pour récupérer tous les contrats d'un utilisateur spécifique avec les détails utilisateur
app.get('/contracts/:userId', (req, res) => {
  const userId = req.params.userId;
  const selectQuery = `
    SELECT contract.*, user.nom AS user_nom, user.prenom AS user_prenom
    FROM contract
    JOIN user ON contract.userId = user.id
    WHERE contract.userId = ?`;
  db.query(selectQuery, userId, (err, result) => {
    // ... Gérez la réponse et les erreurs
  });
});

// Route pour récupérer tous les partenaires d'un contrat spécifique
app.get('/partners/:contractId', (req, res) => {
  const contractId = req.params.contractId;
  const selectQuery = 'SELECT * FROM partenaire WHERE contractId = ?';
  db.query(selectQuery, contractId, (err, result) => {
    // ... Gérez la réponse et les erreurs
  });
});

// Route pour récupérer toutes les contributions d'un contrat spécifique
app.get('/contributions/:contractId', (req, res) => {
  const contractId = req.params.contractId;
  const selectQuery = 'SELECT * FROM contribution WHERE contractId = ?';
  db.query(selectQuery, contractId, (err, result) => {
    // ... Gérez la réponse et les erreurs
  });
});

// Route pour récupérer tous les détails d'un contrat avec ses partenaires et contributions
app.get('/contractDetails/:contractId', (req, res) => {
  const contractId = req.params.contractId;
  const selectQuery = `
    SELECT contract.*, partenaire.*, contribution.*
    FROM contract
    LEFT JOIN partenaire ON contract.contractId = partenaire.contractId
    LEFT JOIN contribution ON contract.contractId = contribution.contractId
    WHERE contract.contractId = ?`;
  db.query(selectQuery, contractId, (err, result) => {
    // ... Gérez la réponse et les erreurs
  });
});

// Logique CRUD 

// Route pour récupérer tous les contrats d'un utilisateur spécifique avec les détails utilisateur
app.get('/userContracts/:userId', (req, res) => {
  const userId = req.params.userId;
  const selectQuery = `
    SELECT contract.*, user.nom AS user_nom, user.prenom AS user_prenom
    FROM contract
    JOIN user ON contract.userId = user.id
    WHERE contract.userId = ?`;
  db.query(selectQuery, userId, (err, result) => {
    if (err) {
      console.error('Erreur lors de la récupération des contrats utilisateur :', err);
      res.status(500).send('Erreur lors de la récupération des contrats utilisateur');
    } else {
      console.log('Contrats utilisateur récupérés avec succès.');
      res.status(200).json(result);
    }
  });
});

// Route pour récupérer un contrat spécifique d'un utilisateur
app.get('/userContract/:userId/:contractId', (req, res) => {
  const { userId, contractId } = req.params;
  const selectQuery = `
    SELECT contract.*, user.nom AS user_nom, user.prenom AS user_prenom
    FROM contract
    JOIN user ON contract.userId = user.id
    WHERE contract.userId = ? AND contract.contractId = ?`;
  db.query(selectQuery, [userId, contractId], (err, result) => {
    // ... Gérez la réponse et les erreurs
  });
});

// Route pour mettre à jour un contrat spécifique d'un utilisateur
app.put('/userContract/:userId/:contractId', (req, res) => {
  const { userId, contractId } = req.params;
  const valuesCData = req.body.values;
  const updateQuery = `
    UPDATE contract 
    SET date=?, nbCopie=?, texte1=?, texte2=?, texte3=?, date2=?, texte4=?, nbPartenairesR=?, tempsNC=?, pays=?, heureSignature=?, nomAvocat=?
    WHERE contractId = ? AND userId = ?`;
  const updateValues = [
      valuesCData.date,
      valuesCData.nbCopie,
      valuesCData.texte1,
      valuesCData.texte2,
      valuesCData.texte3,
      valuesCData.date2,
      valuesCData.texte4,
      valuesCData.nbPartenairesR,
      valuesCData.tempsNC,
      valuesCData.pays,
      valuesCData.heureSignature,
      valuesCData.nomAvocat,
      contractId,
      userId
  ];

  db.query(updateQuery, updateValues, (err, result) => {
    // ... Gérez la réponse et les erreurs
  });
});

// Route pour supprimer un contrat spécifique d'un utilisateur
app.delete('/userContract/:userId/:contractId', (req, res) => {
  const { userId, contractId } = req.params;
  const deleteQuery = 'DELETE FROM contract WHERE contractId = ? AND userId = ?';
  db.query(deleteQuery, [contractId, userId], (err, result) => {
    // ... Gérez la réponse et les erreurs
  });
});

const PORT = process.env.PORT || 8081;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});

