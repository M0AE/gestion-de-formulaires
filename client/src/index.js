import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

/*
	Auteur : Rosat Aurélien
	Date : 13/12/2023
	Description : Render de l'application avec ReactDom
*/

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
