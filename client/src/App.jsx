import {BrowserRouter, Routes, Route} from 'react-router-dom';
import './style.css';
import Home from './components/Home';
import Register from './components/register';
import Login from './components/Login';
import Contract from './components/contract';

/*
	Auteur : Rosat Aurélien
	Date : 13/12/2023
	Description : Root du Project
*/

function App() {
  return (
    <div className="App">
      <BrowserRouter>
      {/* Différentes Routes du Project*/}
        <Routes>
          <Route path= '/' element={<Login/>}></Route>
          <Route path= '/login' element={<Login/>}></Route>
          <Route path= '/Home' element={<Home/>}></Route>
          <Route path='/Contract' element={<Contract/>}></Route>
          <Route path='/register'element={<Register/>}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
