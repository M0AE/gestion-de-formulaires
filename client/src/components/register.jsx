import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";

/*
	Auteur : Rosat Aurélien
	Date : 13/12/2023
	Description : Page Register
*/

const Register = () => {

  //récupération des données dynamique
  const [values, setValues] = useState({
    nom: '',
    prenom :'',
    mdp: '',
    email: ''
  });
  
//Envoie des données au serveur 
const navigate = useNavigate();
const handleSubmit = (event) => {
  event.preventDefault();

  fetch('http://localhost:8081/register', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(values)
  })
  .then(res => {
    if (!res.ok) {
      throw new Error('Network response was not ok');
    }
    return res.json(); // Récupérer la réponse JSON depuis la requête
  })
  .then(data => {
    if(data.status === "Success"){
      navigate('/login');
    } else {
      alert("Error");
    }
  })
  .catch(err => console.log(err));
}
  
  return (
    <div className="bg-teal-500">
      <main className="h-screen flex items-center justify-center">
        <form onSubmit={handleSubmit} className="bg-white flex rounded-lg w-1/2 font-latoRegular">
          <div className="flex-1 text-gray-700 p-20">
            <h1 className="text-2xl pb-2 font-bold">Bienvenue sur le site de gestion de formulaires</h1>
            <p className="text-lg text-gray-500">
              Inscrivez vous sur nôtre site
              pour créer et stocker des contracts, afin de faciliter les échanges avec vos partenaires.
            </p>
            <div className="mt-6">
              <div className="pb-4">
                <label className="block font-bold text-sm pb-2" htmlFor="name">Nom</label>
                <input className="border-2 border-gray-300 p-2 rounded-md w-1/2 focus:border-teal-500 focus:outline-none focus:ring-0 placeholder:text-sm " 
                type="text" 
                name="name" 
                onChange={(e) => setValues({...values,nom : e.target.value})} // récupération de la valeur dans le input
                placeholder="Entrer votre Identifiant"
                />
              </div>
              <div className="pb-4">
                <label className="block font-bold text-sm pb-2" htmlFor="name">Prénom</label>
                <input className="border-2 border-gray-300 p-2 rounded-md w-1/2 focus:border-teal-500 focus:outline-none focus:ring-0 placeholder:text-sm " 
                type="text" 
                name="prénom" 
                onChange={(e) => setValues({...values,prenom : e.target.value})} // récupération de la valeur dans le input
                placeholder="Entrer votre Identifiant"
                />
              </div>
              <div className="pb-4">
                <label className="block font-bold text-sm pb-2" htmlFor="pwd" >Mot de passe</label>
                <input className="border-2 border-gray-300 p-2 rounded-md w-1/2 focus:border-teal-500 focus:outline-none focus:ring-0 placeholder:text-sm" 
                type="password" 
                name="password"  
                onChange={(e) => setValues({...values,mdp : e.target.value})}
                placeholder="Entrer votre Mot de passe"
                />
              </div>
              <div className="pb-4">
                <label className="block font-bold text-sm pb-2" htmlFor="email" >Email</label>
                <input className="border-2 border-gray-300 p-2 rounded-md w-1/2 focus:border-teal-500 focus:outline-none focus:ring-0 placeholder:text-sm"
                type="email"
                name="email"  
                onChange={(e) => setValues({...values,email : e.target.value})}
                placeholder="Entrer votre Email"/>
              </div>
              <div className="pb-4">
                <label className="block font-bold text-sm pb-2" htmlFor="terms" >Contract d'utilisation</label>
                <div className="flex items-center gap-2">
                  <input className=" relative peer shrink-0 appearance-none w-4 h-4 border-2 border-teal-500 rounded-sm bg-white mt-1 checked:bg-teal-500 checked:border-0" type="checkbox" name="terms" value="checked" />
                  <p className="text -sm font-bold text-gray-500">
                    &nbsp;J'accepte que mes données soit collectées et exploitées
                  </p>
                </div>
              </div>
              {/* Pour valider la regsitration*/}
              <button type="submit" className="bg-teal-500 font-bold text-sm text-white py-3 mt-6 rounded-lg w-full">SignUp</button> 
              <div className="bg-teal-500 font-bold text-sm text-white py-3 mt-6 rounded-lg w-full text-center">
              <Link to="/login">Login</Link> {/* root vers la page login*/}
              </div>
            </div>
          </div>
          <div className=" relative flex-1">
            <img src="form.png" alt="form" className="object-cover rounded-lg"/>
          </div>
        </form>
      </main> 
    </div>
  ); 
}

export default Register;

