import React from 'react';
import { useState } from 'react';
import axios from 'axios';
/*
	Auteur : Rosat Aurélien
	Date : 13/12/2023
	Description : Nomanclature Contract
*/
function Contract() {
  {/* values table contract*/ }
  const [valuesC, setValuesC] = useState({
    date : '',
    nbCopie :'',
    texte1 : '',
    texte2 : '',
    texte3 : '',
    date2 :'',
    texte4 : '',
    nbPartenairesR : '',
    tempsNC : '',
    pays : '',
    heureSignature :'',
    nomAvocat : ''
  });
 {/* values table partenaire*/ }
  const [valuesP , setValuesP] = useState({
    nom :'',
    prénom : '',
    email :''
});
{/* values table contribution*/ }
const [valuesCO, setValuesCO] = useState({
  text : ''
})

const sendDataToServer = async () => {
  try {
      // Envoi des valeurs vers la route contract
      await axios.post('http://localhost:8081/contract', { values: valuesC });
      console.log('Données de valuesC envoyées avec succès.');

      // Envoi des valeurs vers la route partenaire
      await axios.post('http://localhost:8081/partner', { values: valuesP });
      console.log('Données de valuesP envoyées avec succès.');

      // Envoi des valeurs vers la route contribution
      await axios.post('http://localhost:8081/contribution', { values: valuesCO });
      console.log('Données de valuesCO envoyées avec succès.');
  } catch (error) {
      console.error('Erreur lors de l\'envoi des données :', error);
  }
};

const handleSubmit = (event) => {
  event.preventDefault(); 

  sendDataToServer(); //envoie les données au serveur
};


  return (
    //Contract
    <div className="bg-teal-500">
      <main className=" flex items-center justify-center">
        <form onSubmit={handleSubmit} className="bg-white flex rounded-lg w-1/2 font-latoRegular my-10">
          <div className="flex-1 text-gray-700 p-20 ">
            <h1 className="text-3xl pb-2 font-bold">CONTRAT DE PARTENARIAT COMMERCIAL</h1>
              <br />
              <p className="text-lg text-gray-500">
                Ce contrat est fait ce jour <input type="date" onChange={(e) => setValuesC({...valuesC,date : e.target.value})}/>
                en <input type="number" onChange={(e) => setValuesC({...valuesC,nbCopie: e.target.value})}/>copies originales, entre :
              </p>
              <br />
              <p className="text-lg text-gray-500"> {/* credentials partenaires*/}
                <input type="text" placeholder='nom' onChange={(e) => setValuesP({...valuesP,nom : e.target.value})}/>
                <input type="text" placeholder='prénom'onChange={(e) => setValuesP({...valuesP,prénom : e.target.value})}/>
                <input type="mail" placeholder='email' onChange={(e) => setValuesP({...valuesP,email : e.target.value})}/>
              </p>
              <br />
            <h1 className="text-2xl pb-2 font-bold">1. NOM DU PARTENARIAT ET ACTIVITE</h1>
              <p className="text-lg text-gray-500">
                1.1 Nature des activités: Les Partenaires cités ci-dessus donnent leur accord d'être
                considérés comme des partenaires commerciaux pour les fins suivantes :
                <input type="text" onChange={(e) => setValuesC({...valuesC,texte1 : e.target.value})}/>
              </p>
              <br />
              <p className="text-lg text-gray-500">
                1.2 Nom: Les Partenaires cités ci-dessus donnent leur accord, pour que le partenariat
                commercial soit exercé sous le nom:
                <input type="text" onChange={(e) => setValuesC({...valuesC,texte2 : e.target.value})}/>
              </p>
              <br />
              <p className="text-lg text-gray-500">
                1.3 Adresse officielle: Les Partenaires cités ci-dessus donnent leur accord pour que le siège
                du partenariat commercial soit:
                <input type="text" onChange={(e) => setValuesC({...valuesC,texte3 : e.target.value})}/>
              </p>
              <br />
            <h1 className="text-2xl pb-2 font-bold">2. TERMES</h1>
              <p className="text-lg text-gray-500">
                2.1 Le partenariat commence le <input type="date" onChange={(e) => setValuesC({...valuesC,date2: e.target.value})}/>et continue jusqu'à la
                fin de cet accord.
              </p>
              <br />
            <h1 className="text-2xl pb-2 font-bold">3. CONTRIBUTION AU PARTENARIAT</h1>
              <p className="text-lg text-gray-500">
                3.1 La contribution de chaque partenaire au capital listée ci-dessous se compose des éléments
                suivants :
              </p>
              <br />
              <p className="text-lg text-gray-500"> {/* faire l'ajout de contributions*/}
                <input type="text" placeholder='contributions' onChange={(e) => setValuesCO({...valuesCO,text : e.target.value})}/>
              </p>
              <br />
            <h1 className="text-2xl pb-2 font-bold">4. RÉPARTITION DES BÉNÉFICES ET DES PERTES</h1>
              <p className="text-lg text-gray-500">
                4.1 Les Partenaires se partageront les profits et les pertes du partenariat commercial de la
                manière suivante : 
                <input type="text" onChange={(e) => setValuesC({...valuesC,texte4 : e.target.value})}/>
              </p>
              <br />
            <h1 className="text-2xl pb-2 font-bold">5. PARTENAIRES ADDITIONNELS</h1>
              <p className="text-lg text-gray-500">
                5.1 Aucune personne ne peut être ajoutée en tant que partenaire et aucune autre autre activité
                ne peut être menée par le partenariat sans le consentement écrit de tous les partenaires.
              </p>
              <br />
            <h1 className="text-2xl pb-2 font-bold">6.MODALITÉS BANCAIRES ET TERMES FINANCIERS</h1>
              <p className="text-lg text-gray-500">
                6.1 Les Partenaires doivent avoir un compte bancaire au nom du partenariat sur lequel les
                chèques doivent être signés par au moins <input type="number" onChange={(e) => setValuesC({...valuesC,nbPartenairesR : e.target.value})}/>des Partenaires.
              </p>
              <br />
              <p className="text-lg text-gray-500">
                6.2 Les Partenaires doivent tenir une comptabilité complète du partenariat et la rendre
                disponible à tous les Partenaires à tout moment.
              </p>
              <br />
            <h1 className="text-2xl pb-2 font-bold" >7. GESTION DES ACTIVITÉS DE PARTENARIAT</h1>
              <p className="text-lg text-gray-500">
                7.1 Chaque partenaire peut prendre part dans la gestion du partenariat.
              </p>
              <br />
              <p className="text-lg text-gray-500">
                7.2 Tout désaccord qui intervient dans l'exploitation du partenariat, sera réglé par les
                partenaires détenant la majorité des parts du partenariat.
              </p>
              <br />
            <h1 className="text-2xl pb-2 font-bold">8. DÉPART D'UN PARTENAIRE COMMERCIAL</h1>
              <p className="text-lg text-gray-500"> 
                8.1 Si un partenaire se retire du partenariat commercial pour une raison quelconque, y
                compris le décès, les autres partenaires peuvent continuer à exploiter le partenariat sous le
                même nom.
              </p>
              <br />
              <p className="text-lg text-gray-500">
                8.2 Le partenaire qui se retire est tenu de donner un préavis écrit d'au moins soixante (60)
                jours de son intention de se retirer et est tenu de vendre ses parts du partenariat commercial.
              </p>
              <br />
              <p className="text-lg text-gray-500">
                8.3 Aucun partenaire ne doit céder ses actions dans le partenariat commercial à une autre
                partie sans le consentement écrit des autres partenaires.
              </p>
              <br />
              <p className="text-lg text-gray-500">
                8.4 Le ou les partenaires restants paieront au partenaire qui se retire, ou au représentant légal
                du partenaire décédé ou handicapé, la valeur de ses parts dans le partenariat, ou : <br/><br />
                (a) la sommede son capital,<br />
                (b) des emprunts impayés qui lui sont dus,<br />
                (c) sa quote-part des bénéfices nets cumulés non distribués dans son compte, et <br />
                (d) son intérêt dans toute plus-value préalablement convenue de la valeur du partenariat par rapport
                à sa valeur comptable.
                Aucune valeur de bonne volonté ne doit être incluse dans la détermination de la valeur des
                parts du partenaire.
              </p>
              <br />
            <h1 className="text-2xl pb-2 font-bold">9. CLAUSE DE NON CONCURRENCE</h1>
              <p className="text-lg text-gray-500">
                9.1 Un partenaire qui se retire du partenariat ne doit pas s'engager directement ou
                indirectement dans une entreprise qui est ou serait en concurrence avec la nature des activités
                actuelles ou futures du partenariat pendant
                <input type="text" onChange={(e) => setValuesC({...valuesC,tempsNC : e.target.value})}/>.
              </p>
              <br />
            <h1 className="text-2xl pb-2 font-bold">10. MODIFICATION DE L’ACCORD DE PARTENARIAT</h1>
              <p className="text-lg text-gray-500">
                10. 1 Ce contrat de partenariat commercial ne peut être modifié sans le consentement écrit de
                tous les partenaires.
              </p>
              <br />
            <h1 className="text-2xl pb-2 font-bold">11. DIVERS</h1>
              <p className="text-lg text-gray-500">
                11.1 Si une disposition ou une partie d'une disposition de la présente convention de
                partenariat commercial est non applicable pour une quelconque raison, elle sera dissociée
                sans affecter la validité du reste de la convention.
              </p>
              <br />
              <p className="text-lg text-gray-500">
                11.2 Cet accord de partenariat commercial lie les partenaires commerciaux et pourra servir à
                leurs héritiers, exécuteurs testamentaires, administrateurs, représentants personnels,
                successeurs et ayants droit respectifs.
              </p>
              <br />
            <h1 className="text-2xl pb-2 font-bold">12. JURIDICTION</h1>
              <p className="text-lg text-gray-500">
                12.1 Le présent contrat de partenariat commercial est régi par les lois de l’État de 
                <input type="text" onChange={(e) => setValuesC({...valuesC,pays : e.target.value})}/>
              </p>

              <p className="text-lg text-gray-500">
                Solennellement affirmé à <input type="text" onChange={(e) => setValuesC({...valuesC,heureSignature : e.target.value})}/>
                Daté de ce jour <input type="date" onChange={(e) => setValuesC({...valuesC,date : e.target.value})}/>
              </p>

              <p className="text-lg text-gray-500">
                Signé, validé et livré en présence de: 
                {valuesP.nom}
                {valuesP.prénom}
              </p>

              <p className="text-lg text-gray-500">
                Par moi: <input type="text" onChange={(e) => setValuesC({...valuesC,nomAvocat : e.target.value})}/>
              </p>
              <button type='submit' className="bg-teal-500 font-bold text-sm text-white py-3 mt-6 rounded-lg w-full">Submit</button>
              </div>
          </form>
      </main>
    </div>
  )
}

export default Contract