import { useState } from 'react';
import { Link, useNavigate} from 'react-router-dom';
import axios from 'axios';

/*
	Auteur : Rosat Aurélien
	Date : 13/12/2023
	Description : Page Login
*/

function Login() {

  const [values, setValues] = useState({
    email: '',
    mdp: ''
  });

  const navigate = useNavigate();
  const handleSubmit = (event) => {
    event.preventDefault();
  
  axios.post('http://localhost:8081/login', values)
  .then(res => {
    console.log(res.data); // Vérifiela réponse du serveur
    if (res.data.Status === 'Success') {
      navigate('/Home'); // Redirection si la connexion réussit
    } else {
      alert('Login failed');
    }
  })
  .catch(err => {
    console.error(err); // Affiche l'erreur reçue lors de la requête
    alert('Une erreur s\'est produite'); // Afficher un message d'erreur générique pour l'utilisateur
  });
  }
  

  return (
    //Page de Login
    <div className="bg-teal-500">
    <main className="h-screen flex items-center justify-center">
      <form onSubmit={handleSubmit} className="bg-white flex rounded-lg w-1/2 font-latoRegular">
        <div className="flex-1 text-gray-700 p-20">
          <h1 className="text-2xl pb-2 font-bold">Bienvenue sur le site de gestion de formulaires</h1>
          <p className="text-lg text-gray-500">Connectez vous pour acceder au gestionnaire</p>
          <div className="mt-6">
          <label className="block font-bold text-sm pb-2" htmlFor="pwd" >Email</label>
          <input
              className="border-2 border-gray-300 p-2 rounded-md w-1/2 focus:border-teal-500 focus:outline-none focus:ring-0 placeholder:text-sm"
              type="email"
              name="email"
              onChange={(e) => setValues({ ...values, email: e.target.value })}
              placeholder="Entrer votre email"
          />

            <div className="pb-4">
              <label className="block font-bold text-sm pb-2" htmlFor="pwd" >Mot de passe</label>
              <input className="border-2 border-gray-300 p-2 rounded-md w-1/2 focus:border-teal-500 focus:outline-none focus:ring-0 placeholder:text-sm" 
              type="password" 
              name="password" 
              onChange={(e) => setValues({...values, mdp: e.target.value})}
              placeholder="Entrer votre Mot de passe"
              />
            </div>
            <div className="bg-teal-500 font-bold text-sm text-white py-3 mt-6 rounded-lg w-full text-center">
              <button type='submit'>Login</button> {/* Pour se connecter*/}
            </div>
            <div className="bg-teal-500 font-bold text-sm text-white py-3 mt-6 rounded-lg w-full text-center">
            <Link to="/register">SignUp</Link> {/* Si on a pas de compte*/}
            </div>
          </div>
        </div>
        <div className=" relative flex-1">
          <img src="form.png" alt=""  className="object-cover rounded-lg"/>
        </div>
      </form>
    </main> 
  </div>
  )
}

export default Login