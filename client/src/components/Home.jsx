import React from 'react';
import { Link} from 'react-router-dom';
/*
	Auteur : Rosat Aurélien
	Date : 13/12/2023
	Description : Dashboard Crud
*/
function Home() {

return (
  //Dashboard CRUD
  <div className='bg-teal-500'>
    <div className='h-screen flex items-center justify-center'>
      <form className='bg-white flex rounded-lg w-1/2 font-latoRegular'>
        <div className='flex-1 text-gray-700 p-20 flex flex-col items-center'>
          <h1 className="text-2xl pb-2 font-bold text-center">Dashboard</h1>
          <div className='bg-teal-500 font-bold text-sm text-white py-3 mt-6 rounded-lg w-full text-center'>
            <Link to="/contract">Create Contract</Link>
          </div>
        </div>
      </form>
    </div>
  </div>
);
}                                                                

export default Home;